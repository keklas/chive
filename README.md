# Chive

## Requirements

- Works only on archlinux and hiveos

## Features
- Installs chia blockchain, plotman (automated plot management tool) and hpool (chia mining pool)
- Seamlessly integrates with a default hiveos install or a fresh archlinux Install
- Manage configurations and monitor plotting processes


# WIP
- Automatically removes temporary plots and starts new plotting jobs
- Automatic update checking (of this script)
- Configuration file for this script

# Installation
```
https://gitlab.com/keklas/chive.git
cd chive
./chive install all
```
